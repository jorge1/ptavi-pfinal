#!/usr/bin/python3
#-*-coding: utf-8-*-

import os
import sys
import time
import socket
import socketserver

from xml.sax import make_parser
from xml.sax.handler import ContentHandler

class XMLUAHandler(ContentHandler):
    def __init__(self):
        self.conf = {}
        self.att = {'account': ['username', 'passwd'],
                    'uaserver': ['ip', 'puerto'],
                    'rtpaudio': ['puerto'],
                    'regproxy': ['ip', 'puerto'],
                    'log': ['path'],
                    'audio': ['path']}

    def startElement(self, name, attrs):
        if name in self.att:
            for att in self.att[name]:
                self.conf[name + "-" + att] = attrs.get(att, '')

    def get_tags(self):
        return self.conf

def write_log(log_file, message):
    now = time.gmtime(time.time() + 3600)
    log = open(log_file, 'a')
    log.write(time.strftime(('%Y%m%d%H%M%S'), now) + ' ' + message + '\n')
    log.close()

class SIPUAHandler(socketserver.DatagramRequestHandler):

    mp32rtp = []

    def handle(self):
        data = self.rfile.read().decode('utf-8')
        address = self.client_address[0] + ':' + str(self.client_address[1])
        write_log(log_file,'Received from ' + address + ': ' + data.replace('\r\n',' '))
        method = data.split()[0]
        print(method,'received')
        if method == 'INVITE':
            rtp_ip = data.split('\r\n')[4].split()[1]
            rtp_port = data.split('\r\n')[7].split()[1]
            self.mp32rtp.append(rtp_ip)
            self.mp32rtp.append(rtp_port)
            trying = 'SIP/2.0 100 Trying\r\n\r\n'
            ringing = 'SIP/2.0 180 Ringing\r\n\r\n'
            ok = 'SIP/2.0 200 OK\r\n'
            sdp_body = 'Content-Type: application/sdp\r\n\r\n'
            sdp_body += 'v=0\r\n'
            sdp_body += 'o=' + tags['account-username'] + ' ' + tags['uaserver-ip'] + '\r\n'
            sdp_body += 's=misesion\r\n'
            sdp_body += 't=0\r\n'
            sdp_body += 'm=audio ' + tags['rtpaudio-puerto'] + ' RTP\r\n'
            request = trying + ringing + ok + sdp_body
            self.wfile.write(bytes(request,'utf-8'))
            write_log(log_file,'Sent to ' + address + ': ')
        elif method == 'BYE':
            self.wfile.write(b'SIP/2.0 200 OK\r\n')
            write_log(log_file,'Sent to ' + address + ': SIP/2.0 200 OK')
            self.mp32rtp = []
        elif method == 'ACK':
            command = './mp32rtp -i ' + self.mp32rtp[0] + ' -p ' + self.mp32rtp[1]
            command += ' < ' + tags['audio-path']
            print('Start RTP...')
            os.system(command)
            print('RTP end.')
        else:
            self.wfile.write(b'SIP/2.0 405 Method Not Allowed\r\n')

if __name__ == '__main__':
    try:
        xml_file = sys.argv[1]
    except:
        sys.exit('usage error: python3 uaserver.py XML_file')

    parser = make_parser()
    xml_list = XMLUAHandler()
    parser.setContentHandler(xml_list)
    parser.parse(open(xml_file))
    tags = xml_list.get_tags()

    ip = tags['uaserver-ip']
    port = tags['uaserver-puerto']
    log_file = tags['log-path']

    try:
        uaserver = socketserver.UDPServer((ip, int(port)), SIPUAHandler)
        print('Listening at ' + ip + ':' + str(port) + '...\r\n')
        write_log(log_file, 'Starting...')
        uaserver.serve_forever()
    except KeyboardInterrupt:
        print('End server in ' + ip + ':' + str(port))
        write_log(log_file, 'Finishing')
