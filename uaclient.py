# !/usr/bin/python3
# -*-coding: utf-8-*-

import os
import sys
import time
import socket

from hashlib import md5
from xml.sax import make_parser
from xml.sax.handler import ContentHandler


def get_digest(nonce, passwd, encoding='utf-8'):
    digest = md5()
    digest.update(bytes(nonce, encoding))
    digest.update(bytes(passwd, encoding))
    digest.digest()
    return digest.hexdigest()


class XMLUAHandler(ContentHandler):
    def __init__(self):
        self.conf = {}
        self.att = {'account': ['username', 'passwd'],
                    'uaserver': ['ip', 'puerto'],
                    'rtpaudio': ['puerto'],
                    'regproxy': ['ip', 'puerto'],
                    'log': ['path'],
                    'audio': ['path']}

    def startElement(self, name, attrs):
        if name in self.att:
            for att in self.att[name]:
                self.conf[name + "-" + att] = attrs.get(att, '')

    def get_tags(self):
        return self.conf


def write_log(log_file, message):
    now = time.gmtime(time.time() + 3600)
    log = open(log_file, 'a')
    log.write(time.strftime(('%Y%m%d%H%M%S'), now) + ' ' +  message + '\n')
    log.close()


def get_sdp(tags):
    sdp_body = 'Content-Type: application/sdp\r\n\r\n'
    sdp_body += 'v=0\r\n'
    sdp_body += 'o=' + tags['account-username'] + ' ' + tags['uaserver-ip']
    sdp_body += '\r\ns=misesion\r\n'
    sdp_body += 't=0\r\n'
    sdp_body += 'm=audio ' + tags['rtpaudio-puerto'] + ' RTP'
    
    return sdp_body


def get_request(method, tags, option=''):

    if method.lower() == 'register':
        request = method.upper() + ' sip:' + tags['account-username'] + ':'
        request += tags['uaserver-puerto'] + ' SIP/2.0\r\n'
        request += 'Expires: ' + option + '\r\n'
    elif method.lower() == 'invite':
        request = method.upper() + ' sip:' + option + ' SIP/2.0\r\n'
        request += get_sdp(tags) + '\r\n'
    elif method.lower() == 'bye':
        request = method.upper() + ' sip:' + option + ' SIP/2.0\r\n'
    else:
        request = method.upper() + ' sip:' + tags['account-username'] + ':'
        request += tags['uaserver-puerto']
        request += ' SIP/2.0\r\n' + option + '\r\n'
    
    return request


if __name__ == '__main__':
    try:
        xml_file = sys.argv[1]
        method = sys.argv[2]
        option = sys.argv[3]
    except:
        sys.exit('usage error: python3 uaclient.py XML_file method option')

    parser = make_parser()
    xml_list = XMLUAHandler()
    parser.setContentHandler(xml_list)
    parser.parse(open(xml_file))
    tags = xml_list.get_tags()

    pr_ip = tags['regproxy-ip']
    pr_port = tags['regproxy-puerto']
    pr_address = pr_ip + ':' + str(pr_port)
    log_file = tags['log-path']

    request = get_request(method, tags, option)

    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        my_socket.connect((pr_ip, int(pr_port)))
        try:
            my_socket.send(bytes(request, 'utf-8'))
            log_mess = 'Sent to ' + pr_address + ': '
            log_mess += request.replace('\r\n',' ')
            write_log(log_file, log_mess)
        except:
            log_mess = 'Error: no server listening at ' + pr_ip + ' port '
            log_mess += str(pr_port)
            write_log(log_file, log_mess)
            sys.exit('Server unreachable')
        data = my_socket.recv(1024).decode('utf-8')
        log_mess = 'Received from ' + pr_address + ': '
        log_mess += data.replace('\r\n',' ')
        write_log(log_file, log_mess)

        trying = 'SIP/2.0 100 Trying' in data
        ringing = 'SIP/2.0 180 Ringing' in data
        ok = 'SIP/2.0 200 OK' in data
        bad_request = 'SIP/2.0 400 Bad Request' in data
        unauthorized = 'SIP/2.0 401 Unauthorized' in data
        user_not_found = 'SIP/2.0 404 User Not Found' in data
        method_not_allowed = 'SIP/2.0 405 Method Not Allowed' in data

        if method.lower() == 'bye' and ok:
            sys.exit('200 OK')

        if unauthorized:
            digest_nonce = data.split('"')[1]
            request = method.upper() + ' sip:' + tags['account-username'] + ':' 
            request += tags['uaserver-puerto'] + ' SIP/2.0\r\n'
            request += 'Expires: ' + option + '\r\n'
            digest_response = get_digest(digest_nonce,tags['account-passwd'])
            request += 'Authorization: Digest response="' + digest_response
            request += '"\r\n'
        else:
            if trying and ringing and ok:
                rtp_ip = data.split('\r\n')[8].split()[1]
                rtp_port = data.split('\r\n')[11].split()[1]
                request = 'ACK sip:' + tags['account-username'] + ' SIP/2.0\r\n'
            else:
                if bad_request:
                    sys.exit('400 Bad Request')
                if user_not_found:
                    sys.exit('404 User not found')
                if method_not_allowed:
                    sys.exit('405 Method not allowed')
                if ok:
                    sys.exit('200 OK')

        my_socket.send(bytes(request, 'utf-8'))
        log_mess = 'Sent to ' + pr_address + ': ' + request.replace('\r\n',' ')
        write_log(log_file, log_mess)

        if 'ACK' in request:
            mp32rtp = './mp32rtp -i ' + rtp_ip + ' -p ' + rtp_port
            mp32rtp += ' < ' + tags['audio-path']
            os.system(mp32rtp)
            sys.exit('RTP finalized')

        data = my_socket.recv(1024).decode('utf-8')
        log_mess = 'Received from ' + pr_address + ': '
        log_mess += data.replace('\r\n',' ')
        write_log(log_file, log_mess)

        ok = 'SIP/2.0 200 OK' in data
        bad_request = 'SIP/2.0 400 Bad Request' in data
        unauthorized = 'SIP/2.0 401 Unauthorized' in data
        user_not_found = 'SIP/2.0 404 User Not Found' in data
        method_not_allowed = 'SIP/2.0 405 Method Not Allowed' in data

        if unauthorized:
            sys.exit('401 Unauthorized')
        if bad_request:
            sys.exit('400 Bad Request')
        if user_not_found:
            sys.exit('404 User not found')
        if method_not_allowed:
            sys.exit('405 Method not allowed')
        if ok:
            sys.exit('200 OK')
