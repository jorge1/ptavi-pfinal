#!/usr/bin/python3
#-*-coding: utf-8-*-

import os
import sys
import json
import time
import random
import socket
import socketserver

from hashlib import md5
from xml.sax import make_parser
from xml.sax.handler import ContentHandler

def write_log(log_file, message):
    now = time.gmtime(time.time() + 3600)
    log = open(log_file, 'a')
    log.write(time.strftime(('%Y%m%d%H%M%S'), now) + ' ' + message + '\n')
    log.close()

def get_digest(nonce,passwd,encoding='utf-8'):
    digest = md5()
    digest.update(bytes(nonce,encoding))
    digest.update(bytes(passwd,encoding))
    digest.digest()
    return digest.hexdigest()

class XMLHandler(ContentHandler):
    def __init__(self):
        self.conf = {}
        self.att = {'server': ['name', 'ip', 'puerto'],
                    'database': ['path', 'passwdpath'],
                    'log': ['path']}

    def startElement(self, name, attrs):
        if name in self.att:
            for att in self.att[name]:
                self.conf[name + "-" + att] = attrs.get(att, '')

    def get_tags(self):
        return self.conf


class SIPRegisterHandler(socketserver.DatagramRequestHandler):

    registered = {}
    passwd = {}
    digest_nonce = {}
    sesions = {}

    def handle(self):
        self.json2register()
        self.get_passwords()
        data = self.rfile.read().decode('utf-8')

        ip = self.client_address[0]
        port = self.client_address[1]
        address =  ip + ':' + str(port) + ': '
        write_log(log_file,'Received from ' + address + data.replace('\r\n', ' '))
        method = data.split()[0]
        if method == 'REGISTER':
            try:
                username = data.split('\r\n')[0].split()[1].split(':')[1]
                if username in self.registered:
                    expires_value = int(data.split('\r\n')[1].split()[-1])
                    if expires_value == 0:
                        print(method,'received, deleted')
                        del self.registered[username]
                    else:
                        print(method,'received, actualized')
                        now = time.gmtime(time.time() + 3600 + expires_value)
                        expires = time.strftime(('%Y/%m/%d %H:%M:%S'), now)
                        self.registered[username][2] = expires
                    self.wfile.write(b'SIP/2.0 200 OK\r\n')
                    write_log(log_file,'Sent to ' + address + 'SIP/2.0 200 OK')
                else:
                    if 'Authorization' in data:
                        digest_response = data.split('"')[1]
                        digest_nonce = self.digest_nonce[username]
                        response = get_digest(digest_nonce,self.passwd[username])
                        if digest_response == response:
                            print(method,'received, authorized')
                            port = int(data.split('\r\n')[0].split()[1].split(':')[-1])
                            expires_value = int(data.split('\r\n')[1].split()[-1])
                            now = time.gmtime(time.time() + 3600 + expires_value)
                            expires = time.strftime(('%Y%m%d%H%M%S'), now)
                            ip = self.client_address[0]
                            self.registered[username] = [ip, port, expires]
                            self.wfile.write(b'SIP/2.0 200 OK\r\n')
                            write_log(log_file,'Sent to ' + address + 'SIP/2.0 200 OK')
                        else:
                            print(method,'received, unauthorized')
                            self.wfile.write(b'SIP/2.0 404 User Not Found\r\n')
                            write_log(log_file,'Sent to ' + address + 'SIP/2.0 404 User Not Found')
                    else:
                        print(method,'received, unauthorized')
                        self.digest_nonce[username] = str(random.randint(0000000000, 9999999999))
                        request = 'SIP/2.0 401 Unauthorized\r\nWWW Authenticate: Digest nonce="' 
                        request += self.digest_nonce[username] + '"\r\n'
                        self.wfile.write(bytes(request,'utf-8'))
                        write_log(log_file,'Sent to ' + address + request.replace('\r\n',' '))
            except:
                print(method,'received, bad request')
                self.wfile.write(b'SIP/2.0 400 Bad Request\r\n')
                write_log(log_file,'Sent to ' + address + 'SIP/2.0 400 Bad Request')
        elif method == 'INVITE':
            try:
                user_dst = data.split('\r\n')[0].split()[1].split(':')[1]
                user_src = data.split('\r\n')[4].split()[0].split('=')[1]
                if user_src in self.registered and user_dst in self.registered:
                    print(method,'received, accepted')
                    sesion_name = data.split('\r\n')[5].split('=')[1]
                    if not sesion_name in self.sesions:
                        self.sesions[sesion_name] = [user_src, user_dst]
                    ip = self.registered[user_dst][0]
                    port = self.registered[user_dst][1]
                    request = self.resent(ip,port,data)
                    if request:
                        self.wfile.write(bytes(request, 'utf-8'))
                else:
                    print(method,'received, user not found')
                    self.wfile.write(b'SIP/2.0 404 User Not Found\r\n')
                    write_log(log_file,'Sent to ' + address + 'SIP/2.0 404 User Not Found')
            except:
                print(method,'received, bad request')
                self.wfile.write(b'SIP/2.0 400 Bad Request\r\n')
                write_log(log_file,'Sent to ' + address + 'SIP/2.0 400 Bad Request')
        elif method == 'BYE':
            try:
                print(method,'received')
                user_src = data.split()[1].split(':')[1]
                user_dst = self.search_client_in_sesions(user_src)
                if user_dst:
                    ip = self.registered[user_dst][0]
                    port = self.registered[user_dst][1]
                    request = self.resent(ip,port,data)
                    if request:
                        self.wfile.write(bytes(request, 'utf-8'))
                        write_log(log_file,'Sent to ' + address + request.replace('\r\n',' '))
            except:
                print(method,'received, bad request')
                self.wfile.write(b'SIP/2.0 400 Bad Request\r\n')
                write_log(log_file,'Sent to ' + address + 'SIP/2.0 400 Bad Request')
        elif method == 'ACK':
            try:
                print(method,'received')
                user_src = data.split()[1].split(':')[1]
                user_dst = self.search_client_in_sesions(user_src)
                if user_dst:
                    ip = self.registered[user_dst][0]
                    port = self.registered[user_dst][1]
                    request = self.resent(ip,port,data)
                    if request:
                        self.wfile.write(bytes(request, 'utf-8'))
                        write_log(log_file,'Sent to ' + address + request.replace('\r\n',' '))
            except:
                print(method,'received, bad request')
                self.wfile.write(b'SIP/2.0 400 Bad Request\r\n')
                write_log(log_file,'Sent to ' + address + 'SIP/2.0 400 Bad Request')
        else:
            print(method,'received, not allowed')
            self.wfile.write(b'SIP/2.0 405 Method Not Allowed\r\n')
            write_log(log_file,'Sent to ' + address + 'SIP/2.0 405 Method Not Allowed')
        self.client_time_out()
        self.register2json()

    def register2json(self):
        with open(tags['database-path'], 'w') as jsonfile:
            json.dump(self.registered, jsonfile, indent=3)

    def json2register(self):
        try:
            with open(tags['database-path'], 'r') as jsonfile:
                self.registered = json.load(jsonfile)
        except:
            pass

    def get_passwords(self):
        try:
            with open(tags['database-passwdpath'], 'r') as jsonfile:
                self.passwd = json.load(jsonfile)
        except:
            pass

    def client_time_out(self):
        try:
            now = time.gmtime(time.time() + 3600)
            dicc = self.registered
            for client in dicc:
                if now >= time.strptime(self.registered[client][2], ('%Y%m%d%H%M%S')):
                    del self.registered[client]
        except:
            pass
    
    def resent(self, ip, port, mess):
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            try:
                my_socket.connect((ip, int(port)))
                my_socket.send(bytes(mess, 'utf-8'))
                write_log(log_file,'Sent to ' + ip + ':' + str(port) + ': ' + mess.replace('\r\n',' '))
                if not 'ACK' in mess:
                    data = my_socket.recv(1024).decode('utf-8')
                    log_mess = 'Received from ' + ip + ':' + str(port) + ': '+ data.replace('\r\n',' ')
                    write_log(log_file,log_mess)
                else:
                    data = ''
            except:
                data = ''
        return data

    def search_client_in_sesions(self, user):
        user_aux = ''
        for sesion in self.sesions:
            if user in self.sesions[sesion]:
                for username in self.sesions[sesion]:
                    if username != user:
                        user_aux = username
        return user_aux
    
    
if __name__ == '__main__':
    try:
        xml_file = sys.argv[1]
    except:
        sys.exit('usage error: python3 proxy_registrar.py XML_file')

    parser = make_parser()
    xml_list = XMLHandler()
    parser.setContentHandler(xml_list)
    parser.parse(open(xml_file))
    tags = xml_list.get_tags()

    ip = tags['server-ip']
    port = tags['server-puerto']
    log_file = tags['log-path']
    pr_name = tags['server-name']

    try:
        server = socketserver.UDPServer((ip, int(port)), SIPRegisterHandler)
        print(pr_name + ' listening at ' + ip + ':' + str(port) + '...\n')
        write_log(log_file, 'Starting...')
        server.serve_forever()
    except KeyboardInterrupt:
        print('\nEnd ' + pr_name)
        write_log(log_file, 'Finishing')
